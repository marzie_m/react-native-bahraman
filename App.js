import React from 'react';
import { Platform, StyleSheet, StatusBar,View,TouchableOpacity} from 'react-native';
import { createBottomTabNavigator, createAppContainer,createStackNavigator,createSwitchNavigator} from 'react-navigation';
import TabBarComponentBahraman from './components/Main/TabBarComponent'
import Icon from 'react-native-vector-icons/FontAwesome';
// import Icon from 'react-native-vector-icons/FontAwesome';
import RF from 'react-native-responsive-fontsize'
import {logRequests} from 'react-native-requests-logger';
import BahramanScreen from './pages/Bahraman/index'
import CookingScreen from './pages/Cooking/CookingScreen'
import Govahiname from './pages/Bahraman/Govahiname'
import AboutBahraman from './pages/Bahraman/AboutBahraman'
import Call from './pages/Bahraman/Call'
import News from './pages/Bahraman/News'
import Quality from './pages/Bahraman/Quality'
import NavigationService from './navigation-service';
import TabBarComponentProducts from './components/Main/TabBarComponentProducts'
// import Product from './pages/Bahraman/Product'
import ProductDetail from './pages/Bahraman/ProductDetail'
import Galary from './pages/Galary'
import DetailCooking from './pages/Cooking/detailCooking'
import Knowledge from './pages/Bahraman/Knowledge'
import Header from './components/Main/Header'
// import Drawer from './components/Main/Drawer'
import TabBarComponentDocuments from './components/Main/TabBarComponentDocuments'
import DetailOfDetailCooking from './pages/Cooking/DetailOfDetailCooking'
import DetailOfDOcuments from './pages/Documents/DetailOfDOcuments'
import Login from './components/Main/Login';
import SplashScreen from './components/Main/SplashScreen';
import SignUp from './components/Main/SignUp';
import checkUser from './components/Main/checkUser';
import TakmileSabtename from './components/Main/TakmileSabtename';

const BahramanNavigator = createStackNavigator(
  {
      BahramanPage: {screen: BahramanScreen,params: { title: 'بهرامن' ,BackPage:'BahramanPage'}},
      Govahiname : {screen : Govahiname,params: { title: 'درباره ی بهرامن' ,BackPage:'BahramanPage'}},
      aboutBahraman :{screen:AboutBahraman,params: { title: 'درباره ی بهرامن' ,BackPage:'BahramanPage'} },
      // productsNav :{screen:ProductNavigator,params: { title: 'محصولات بهرامن' ,BackPage:'BahramanPage'}},
      products :{screen:TabBarComponentProducts,params: { title: 'محصولات بهرامن' ,BackPage:'BahramanPage'}},
      productsDetail :{screen:ProductDetail,params: { title: 'محصولات بهرامن' ,BackPage:'products'}},
      call:{screen :Call,params: { title: 'بهرامن' ,BackPage:'BahramanPage'}},
      news :{screen :News,params: { title: 'بهرامن' ,BackPage:'BahramanPage'}},
      quality :{screen:Quality,params: { title: 'بهرامن' ,BackPage:'BahramanPage'}},
      knowledge :{screen:Knowledge,params: { title: 'شناسایی انواع زعفران' ,BackPage:'BahramanPage'}},

    },
  {
    initialRouteName: 'BahramanPage',
    defaultNavigationOptions : {
      header : props => 
      <View>
        <Header title={props.scene.route.params.title} BackPage='BahramanPage' color='white' textColor='#a7a7a7'/>
        <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,right:20}}
        onPress={()=> props.scene.route.params.isDrawerOpen ? props.scene.route.params.closeDrawer() : props.scene.route.params.openDrawer()}>
          <Icon name={props.scene.route.params.isDrawerOpen ? "times" : "bars"} color={"#aaaaaa"} size={RF(2.5)}/>
        </TouchableOpacity>
      </View>
    }
  },    
);

// const ProductNavigator = createStackNavigator(
//   {
//     products :{screen:TabBarComponentProducts,params: { title: 'محصولات بهرامن' ,BackPage:'BahramanPage'}},
//     productsDetail :{screen:ProductDetail,params: { title: 'محصولات بهرامن' ,BackPage:'products'}},
//   },
//   {
//     initialRouteName: 'products',
//     defaultNavigationOptions : {
//       header : props => 
//       <Header title={props.scene.route.params.title} BackPage={props.scene.route.params.BackPage} color='white' textColor='#a7a7a7'/>
//     }
//   }
// );

 const CookingNavigator = createStackNavigator(
  {
    CookingPage: {screen: CookingScreen,params: { title: 'اشپزی' ,BackPage:'BahramanPage'}},
    DetailCooking:{screen: DetailCooking,params: { title: 'اشپزی' ,BackPage:'CookingPage'}},
    detailOfDetailCooking:{screen: DetailOfDetailCooking,params: { title: 'اشپزی' ,BackPage:'DetailCooking'}}
  },
  {
    initialRouteName: 'CookingPage',
    defaultNavigationOptions : {
      header : props => 
      <View>
        <Header title={props.scene.route.params.title} BackPage={props.scene.route.params.BackPage} color='rgb(178,44,72)' textColor='white'/>
        {/* /* // <View style={[{position:'relative',backgroundColor:'rgb(178,44,72)'},styles.topBar]}><Text style={[{color:'#969696'},styles.topBarText]}>
        // {props.scene.route.params.title}
        // </Text> */}
        <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,right:20}}
        onPress={()=> props.scene.route.params.isDrawerOpen ? props.scene.route.params.closeDrawer() : props.scene.route.params.openDrawer()}>
          <Icon name={props.scene.route.params.isDrawerOpen ? "times" : "bars"} color={"#aaaaaa"} size={RF(2.5)}/>
        </TouchableOpacity>
      </View>

       /* <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,left:20}}
      onPress={()=>props.navigation.goBack()}>
        <Icon name='chevron-left' color={"#aaaaaa"} size={RF(2.5)}/>
      </TouchableOpacity>
      </View>  */
    }
  }
);
const GlarayNavigator = createStackNavigator(
  {
    GalaryPage: {screen: Galary,params: { title: 'گالری' ,BackPage:'BahramanPage'}},
  },
  {
    initialRouteName: 'GalaryPage',
    defaultNavigationOptions : {
      header : props => 
      <View>
        <Header title={props.scene.route.params.title} BackPage={props.scene.route.params.BackPage} color='rgb(178,44,72)' textColor='white'/>
        <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,right:20}}
        onPress={()=> props.scene.route.params.isDrawerOpen ? props.scene.route.params.closeDrawer() : props.scene.route.params.openDrawer()}>
          <Icon name={props.scene.route.params.isDrawerOpen ? "times" : "bars"} color={"#aaaaaa"} size={RF(2.5)}/>
        </TouchableOpacity>
      </View>
    }
  }
);

const DocumentsNavigator = createStackNavigator(
  {
    DocumentsPage: {screen: TabBarComponentDocuments,params: { title: 'شناسایی انواع زعفران' ,BackPage:'BahramanPage'}},
    DetailOfDOcuments :{screen:DetailOfDOcuments,params:{ title: 'شناسایی انواع زعفران' ,BackPage:'DocumentsPage'}},
    // documentsCharGooshe :{screen:DocumentsCharGooshe,params:{ title: 'شناسایی انواع زعفران' ,BackPage:'DocumentsPage'}},
    // documentsViki :{screen:DocumentsViki,params:{ title: 'شناسایی انواع زعفران' ,BackPage:'DocumentsPage'}},
    // allOfDocuments :{screen:AllOfDocuments,params:{ title: 'شناسایی انواع زعفران' ,BackPage:'DocumentsPage'}} 
  },
  {
    initialRouteName: 'DocumentsPage',
    defaultNavigationOptions : {
      header : props => 
      <View>
        <Header title={props.scene.route.params.title} BackPage={props.scene.route.params.BackPage} color='rgb(178,44,72)' textColor='white'/>
        <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,right:20}}
          onPress={()=> props.scene.route.params.isDrawerOpen ? props.scene.route.params.closeDrawer() : props.scene.route.params.openDrawer()}>
            <Icon name={props.scene.route.params.isDrawerOpen ? "times" : "bars"} color={"#aaaaaa"} size={RF(2.5)}/>
        </TouchableOpacity>
      </View>
    }
  }
);


// const DrawerNavigatorExample = createDrawerNavigator({


const TabNavigator = createBottomTabNavigator({
  Bahraman: BahramanNavigator,
  Settings: CookingNavigator,
  galary: GlarayNavigator,
  documents:DocumentsNavigator

},
{
  tabBarComponent: props =>
  <TabBarComponentBahraman
    {...props}
  />
},
{defaultNavigationOptions : 
  {
  header : props => 
  <Header title={props.scene.route.params.title} BackPage='Bahraman' color='white' textColor='#a7a7a7'/>
  }
},

// {navigationOptions: () => ({header: null})}
);


const RootStack = createSwitchNavigator({
  // page0:checkUser,
  page1:SplashScreen,
  page2:Login,
  page3:SignUp,
  page4:TabNavigator,
  page5:TakmileSabtename
},
{defaultNavigationOptions : 
  {
  header : props => 
  <Header title={props.scene.route.params.title} BackPage='Bahraman' color='white' textColor='#a7a7a7'/>
  }
}
)


const AppContainer = createAppContainer(RootStack);
class App extends React.Component {
  constructor(props) {
    super(props);
    logRequests()
  }
  
  render() {
    return (
      <AppContainer
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}



styles = StyleSheet.create({
  topBarText: {
    // fontFamily:'Mj_Flow',
    fontSize:RF(2.4),
    textAlign:'center'
  },
  topBar: {
    width:'100%',
    paddingHorizontal:10,
    paddingTop:Platform.OS == "ios" ? 38 : StatusBar.currentHeight,
    paddingBottom:10
  }
})
export default App;



