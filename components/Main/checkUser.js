import React, { Component } from 'react';

class checkUser extends Component {
    constructor(){
        super();
    }
    componentDidMount(){
        this._getData()
    }
    _getData = async() => {
        try {
            const phone = await AsyncStorage.getItem('phone');
            const password = await AsyncStorage.getItem('password');
            if (phone != null && phone != "" && password != null && password != "") {
                  this.login(phone,password);
            } else {
              this.props.navigation.navigate('SignIn');
            }
           } catch (error) {
              this.props.navigation.navigate('SignIn');
           }
  
    }

    login(phone,password) {
        axios.post(`${this.state.BASE_URL}auth/sign_in/`,{
        user: {
                phone: phone,
                password: password,
            }
        })
        .then(res=>{
            //set user info to storage
            AsyncStorage.setItem('token', res.data.token);
            AsyncStorage.setItem('first_name', res.data.user.first_name);
            AsyncStorage.setItem('last_name', res.data.user.last_name);
            AsyncStorage.setItem('phone', res.data.user.phone);
            AsyncStorage.setItem('password', password);
        })
        .then(sth=>{
            this.props.navigation.navigate('App')
        })
        .catch(e=>{
            this.props.navigation.navigate('SignIn');
        })
    }


    render() {
        return (
            <div>
                
            </div>
        );
    }
}

export default checkUser;