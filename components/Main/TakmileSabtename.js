import React, { Component } from 'react';
import RF from 'react-native-responsive-fontsize'
import {
  Platform,
  StyleSheet,
  Keyboard,
  Text,
  View,
  Alert,
  TextInput,
  ImageBackground,
  TouchableOpacity,
  AsyncStorage,
  AppRegistry,
  TouchableWithoutFeedback
} from 'react-native';
// import ScannerPage from './ScannerPage';
import axios from 'axios';

export default class TakmileSabtename extends Component{

    constructor(){
        super();
        this.state = {
            error: '',
            first_name: '',
            last_name: '',
            email:'',
            // goTo:''

        }
    }


    requestLoginFromApi(){
        if(this.state.first_name != "" && this.state.last_name != "" && this.state.email != "") {
        axios.post(`http://5.253.27.167:8000/api/v1/auth/login/`,{
            user:{
                    first_name:this.state.first_name,
                    last_name:this.state.last_name,
                    email:this.state.email
                }},
            {
                headers:{
                    'content-type': 'application/json',
                    'Authorization':'Token 45ad8b095007b39a59542ceac38ef90aace0774f'
                }  
            } 
        )
        .then(res=>{
            console.log(res.data.user)
            // if(res.data.user.first_name == null || res.data.user.last_name == null){
            //     AsyncStorage.multiSet([['phone', this.state.phone],['is_admin',res.data.user.is_admin.toString()],['token',res.data.token]])


            //     this.setState({goTo:'page5'})
            // }
            // else{
                AsyncStorage.multiSet([['first_name',res.data.user.first_name],['last_name',res.data.user.last_name],['email'],res.data.user.email])
            //     this.setState({goTo:'page4'})
            // }
            
        })
        .then(
            ()=>this.props.navigation.navigate('page4')
        )
        .catch(e => {
            this.setState({error:'failed to login!'})
            console.log(e)
            }
        )
        } else {
            this.setState({
                error:'Please fill all fields!'
            })
        }
        
    }

    render() {
        return (
            <View style={styles.center}>
             <TextInput
                clearButtonMode={'while-editing'}
                keyboardAppearance={"dark"}
                returnKeyType="next"
                style={styles.inputStyle}
                placeholder="first name"
                autoCapitalize = 'none'
                onChangeText={(text) => this.setState({first_name: text})}
                onSubmitEditing={() => { this.secondTextInput.focus(); }}
                blurOnSubmit={false}
                enablesReturnKeyAutomatically
                pointerEvents="auto"
                value={this.state.first_name}
            />  

            <TextInput
                clearButtonMode={'while-editing'}
                keyboardAppearance={"dark"}
                returnKeyType="next"
                style={styles.inputStyle}
                placeholder="last name"
                autoCapitalize = 'none'
                onChangeText={(text) => this.setState({last_name: text})}
                onSubmitEditing={() => { this.secondTextInput.focus(); }}
                blurOnSubmit={false}
                enablesReturnKeyAutomatically
                pointerEvents="auto"
                value={this.state.last_name}
            /> 
            <TextInput
                clearButtonMode={'while-editing'}
                keyboardAppearance={"dark"}
                returnKeyType="next"
                style={styles.inputStyle}
                placeholder="Email"
                autoCapitalize = 'none'
                onChangeText={(text) => this.setState({email: text})}
                onSubmitEditing={() => { this.secondTextInput.focus(); }}
                blurOnSubmit={false}
                enablesReturnKeyAutomatically
                pointerEvents="auto"
                value={this.state.email}
            />
            <TouchableOpacity
            style={styles.enter} 
                onPress={()=>this.requestLoginFromApi()} 
            >
                <Text style={{textAlign:'center' ,color:'#F4F8FB' ,fontSize:RF(2.8),}}>Log in</Text>
            </TouchableOpacity>
            <Text style={{textAlign:'center' ,color:'#F4F8FB' ,fontSize:RF(2.5),marginTop:20}}>{this.state.error}</Text>
            </View>
        )
    }
}
const styles=StyleSheet.create({
    username:{
      // backgroundColor:'#e6e6ff'
    },
     enter:{
          // flex:1,
          width:'90%',
          paddingVertical:16,
          justifyContent:'center' ,
          alignItems:'center' ,
          marginTop:10,
          marginLeft:'10%',
          marginRight:'10%',
          borderRadius:5,
          backgroundColor:'#2E4FAE'
     },
     inputStyle:{
          // backgroundColor:'#3F6BE8',
        //   backgroundColor:'rgba(63,107,232,1)',
          width:'90%',
          textAlign: 'center',
          paddingVertical:15,
          marginTop:10,
          borderRadius:7,
          borderColor:'#2E4FAE',
          borderWidth:1,
          fontSize:RF(3.1),
          color:'black'
     },
     center:{
          flex:1,
          width:'100%',
        //   top:105,
          justifyContent:'center' ,
          alignItems:'center',
          padding: 20,
          backgroundColor:'rgb(238,237,239)',
          // left:'10%',
        //   marginTop:20,
          // position:'absolute',
      }
  })