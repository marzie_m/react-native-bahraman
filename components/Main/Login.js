import React, { Component } from 'react';
import RF from 'react-native-responsive-fontsize'
import {
  Platform,
  StyleSheet,
  Keyboard,
  Text,
  View,
  Alert,
  TextInput,
  ImageBackground,
  TouchableOpacity,
  AsyncStorage,
  AppRegistry,
  TouchableWithoutFeedback
} from 'react-native';
// import ScannerPage from './ScannerPage';
import axios from 'axios';

export default class Login extends Component{

    constructor(){
        super();
        this.state = {
            phone: '',
            code:'',
            error: '',
            auth_state: 'check',
            first_name: '',
            last_name: '',
            email:'',
            goTo:''

        }
    }


    requestLoginFromApi(){
        if(this.state.phone.length == 11) {
        axios.post(`http://5.253.27.167:8000/api/v1/auth/login/`,{
                // user:{
                    'phone':this.state.phone,
                    'code':this.state.code,
                    'first_name':this.state.first_name,
                    'last_name':this.state.last_name,
                    'email':this.state.email
                },
            {
                headers:{
                    'content-type': 'application/json',
                    'Authorization':'Token 45ad8b095007b39a59542ceac38ef90aace0774f'
                }  
            } 
        )
        .then(res=>{
            console.log(res.data)
            // if(res.data.user.first_name == "" || res.data.user.last_name == "" || res.data.user.email == ""){
                // AsyncStorage.multiSet([['phone', this.state.phone],['is_admin',res.data.user.is_admin.toString()],['token',res.data.token]])
                // this.setState({goTo:'page5'})
                // this.props.navigation.navigate('page5')
            // }
            // else{
                // AsyncStorage.multiSet([['phone', this.state.phone],['first_name',res.data.user.first_name],['last_name',res.data.user.last_name],['is_admin',res.data.user.is_admin.toString()],['token',res.data.token],['email'],res.data.user.email])
                // this.setState({goTo:'page4'})
                // this.props.navigation.navigate('page4')
            // }
            
        })
        .then(
            ()=>this.props.navigation.navigate('page4')
        )
        .catch(e => {
            this.setState({error:'failed to login!'})
            console.log(e)
            }
        )
        } else {
            this.setState({
                error:'Please fill all fields!'
            })
        }
        
    }
    render() {
        return (
            <View style={styles.center}>
           {/* <Icon color="white" style={{marginLeft:'auto',marginRight:'auto',marginBottom:40}} size={RF(13)} name='p'/> */}
            <TextInput
                clearButtonMode={'while-editing'}
                keyboardAppearance={"dark"}
                returnKeyType="next"
                style={styles.inputStyle}
                placeholder="phone"
                autoCapitalize = 'none'
                onChangeText={(text) => this.setState({phone: text})}
                onSubmitEditing={() => { this.secondTextInput.focus(); }}
                blurOnSubmit={false}
                enablesReturnKeyAutomatically
                pointerEvents="auto"
                value={this.state.phone}
            />

            <TextInput
                clearButtonMode={'while-editing'}
                keyboardAppearance={"dark"}
                returnKeyType="next"
                style={styles.inputStyle}
                placeholder="code"
                autoCapitalize = 'none'
                onChangeText={(text) => this.setState({code: text})}
                onSubmitEditing={() => { this.secondTextInput.focus(); }}
                blurOnSubmit={false}
                enablesReturnKeyAutomatically
                pointerEvents="auto"
                value={this.state.code}
            /> 
            {/* <TextInput
                clearButtonMode={'while-editing'}
                keyboardAppearance={"dark"}
                returnKeyType="next"
                style={styles.inputStyle}
                placeholder="first name"
                autoCapitalize = 'none'
                onChangeText={(text) => this.setState({first_name: text})}
                onSubmitEditing={() => { this.secondTextInput.focus(); }}
                blurOnSubmit={false}
                enablesReturnKeyAutomatically
                pointerEvents="auto"
                value={this.state.first_name}
            />  

            <TextInput
                clearButtonMode={'while-editing'}
                keyboardAppearance={"dark"}
                returnKeyType="next"
                style={styles.inputStyle}
                placeholder="last name"
                autoCapitalize = 'none'
                onChangeText={(text) => this.setState({last_name: text})}
                onSubmitEditing={() => { this.secondTextInput.focus(); }}
                blurOnSubmit={false}
                enablesReturnKeyAutomatically
                pointerEvents="auto"
                value={this.state.last_name}
            /> 
            <TextInput
                clearButtonMode={'while-editing'}
                keyboardAppearance={"dark"}
                returnKeyType="next"
                style={styles.inputStyle}
                placeholder="Email"
                autoCapitalize = 'none'
                onChangeText={(text) => this.setState({email: text})}
                onSubmitEditing={() => { this.secondTextInput.focus(); }}
                blurOnSubmit={false}
                enablesReturnKeyAutomatically
                pointerEvents="auto"
                value={this.state.email}
            />  */}
   
            <TouchableOpacity
            style={styles.enter} 
                onPress={()=>this.requestLoginFromApi()} 
            >
                <Text style={{textAlign:'center' ,color:'#F4F8FB' ,fontSize:RF(2.8),}}>Log in</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>this.props.navigation.navigate('page3')}>
                <Text style={{textAlign:'center' ,color:'#F4F8FB' ,fontSize:RF(2.8),}}>sign up</Text>
            </TouchableOpacity>
            <Text style={{textAlign:'center' ,color:'#F4F8FB' ,fontSize:RF(2.5),marginTop:20}}>{this.state.error}</Text>
        </View>

        )
    }
    

}
const styles=StyleSheet.create({
    username:{
      // backgroundColor:'#e6e6ff'
    },
     enter:{
          // flex:1,
          width:'90%',
          paddingVertical:16,
          justifyContent:'center' ,
          alignItems:'center' ,
          marginTop:10,
          marginLeft:'10%',
          marginRight:'10%',
          borderRadius:5,
          backgroundColor:'#2E4FAE'
     },
     inputStyle:{
          // backgroundColor:'#3F6BE8',
        //   backgroundColor:'rgba(63,107,232,1)',
          width:'90%',
          textAlign: 'center',
          paddingVertical:15,
          marginTop:10,
          borderRadius:7,
          borderColor:'#2E4FAE',
          borderWidth:1,
          fontSize:RF(3.1),
          color:'black'
     },
     center:{
          flex:1,
          width:'100%',
        //   top:105,
          justifyContent:'center' ,
          alignItems:'center',
          padding: 20,
          backgroundColor:'rgb(238,237,239)',
          // left:'10%',
        //   marginTop:20,
          // position:'absolute',
      }
  })
  


// import React, { Component } from 'react';
// import {View,Text} from 'react-native'

// class Login extends Component {
//     render() {
//         return (
//             <View style={{backgroundColor:'white'}}>
//                 <Text>login</Text>
//             </View>
//         );
//     }
// }

// export default Login;






// import React, { Component } from 'react';
// import RF from 'react-native-responsive-fontsize'
// import {
//   Platform,
//   StyleSheet,
//   Keyboard,
//   Text,
//   View,
//   Alert,
//   TextInput,
//   ImageBackground,
//   TouchableOpacity,
//   AsyncStorage,
//   AppRegistry,
//   TouchableWithoutFeedback
// } from 'react-native';
// // import ScannerPage from './ScannerPage';
// import axios from 'axios';

// export default class Login extends Component{

//     constructor(){
//         super();
//         this.state = {
//             phone: '',
//             code:'',
//             error: '',
//             auth_state: 'check',
//             first_name: '',
//             last_name: '',

//         }
//     }
//     render(){
//         return(
//             <View style={styles.form}>

//             <Text style={styles.title}>
//                 {this.state.auth_state == "check" ? "ورود یا ثبت نام :" : null}
//                 {this.state.auth_state == "login" ? "ورود :" : null}
//                 {this.state.auth_state == "register" ? "تکمیل ثبت نام :" : null}
//                 {this.state.auth_state == "code" ? "تایید تلفن همراه :" : null}
//             </Text>

//             {/* CHECK OR LOGIN */}
//             {this.state.auth_state == "check" || this.state.auth_state == "login" ? 
//             <TextInput
//                 inputAccessoryViewID={inputAccessoryViewID}
//                 style={[styles.input,{marginTop:5,textAlign:'center',direction:'ltr'}]}
//                 editable={this.state.is_loaded}
//                 clearButtonMode={'while-editing'}
//                 placeholder="تلفن همراه"
//                 // value={this.state.phone}
//                 keyboardType="number-pad"
//                 onChangeText={(text) => this.setState({phone: text})}
//                 keyboardAppearance={"dark"}
//                 autoCorrect={false}
//             /> : null }

//             {/* CODE */}
//             {this.state.auth_state == "code" ? 
//             <TextInput
//                 inputAccessoryViewID={inputAccessoryViewID}
//                 style={[styles.input,{marginBottom:5,textAlign:'center',direction:'ltr'}]}
//                 editable={this.state.is_loaded}
//                 clearButtonMode={'while-editing'}
//                 placeholder="کد فعالسازی"
//                 // value={this.state.phone}
//                 keyboardType="number-pad"
//                 onChangeText={(text) => this.setState({code: text})}
//                 keyboardAppearance={"dark"}
//                 autoCorrect={false}
//             /> : null }


//             {/* REGISTER */}
//             {this.state.auth_state == "register" ? 
//                 <TextInput
//                     autoCorrect={false}
//                     editable={this.state.is_loaded}
//                     keyboardAppearance={"dark"}
//                     inputAccessoryViewID={inputAccessoryViewID}
//                     style={[styles.input,{marginTop:0,textAlign:'center',direction:'rtl'}]}
//                     editable={this.state.is_loaded}
//                     placeholder="نام"
//                     onChangeText={(text) => this.setState({first_name: text})}
//             /> : null }


//             {this.state.auth_state == "register" ? 
//             <TextInput
//                 autoCorrect={false}
//                 keyboardAppearance={"dark"}
//                 editable={this.state.is_loaded}
//                 inputAccessoryViewID={inputAccessoryViewID}
//                 style={[styles.input,{marginTop:10,textAlign:'center',direction:'rtl'}]}
//                 editable={this.state.is_loaded}
//                 placeholder="نام خانوادگی"
//                 onChangeText={(text) => this.setState({last_name: text})}
//             /> : null }


// <TouchableOpacity
//                 onPress={
//                     ()=>{
//                         switch(this.state.auth_state) {
//                             case 'check' : this.check();break;
//                             case 'code' : this.code();break;
//                             case 'register' : this.register();break;
//                             case 'login' : this.login();break;
//                             default: break;
//                         }
//                     }
//                 }
//             activeOpacity={.7} style={[styles.submit,{backgroundColor: this.state.is_loaded ? '#20201A' : '#B89F09'}]}>
//           <Text style={styles.submitText}>

//                 {this.state.auth_state == "check" && this.state.is_loaded ? "ورود" : null}
//                 {this.state.auth_state == "login" && this.state.is_loaded ? "ورود" : null}
//                 {this.state.auth_state == "register" && this.state.is_loaded ? "ثبت نام" : null}
//                 {this.state.auth_state == "code" && this.state.is_loaded ? "تایید کد" : null}
//                 {!this.state.is_loaded ? "کمی صبر کنید" : null}

//           </Text>
//         </TouchableOpacity>




//             </View>

//         )
//     }
// }