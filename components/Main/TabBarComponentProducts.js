import React, { Component } from 'react'
import {StyleSheet, Text, View,TouchableOpacity,Dimensions,ScrollView,FlatList} from 'react-native'
const WIN = Dimensions.get('window')
import Icon from 'react-native-vector-icons/FontAwesome';
import RF from 'react-native-responsive-fontsize'
import Product from './../../pages/Bahraman/Product'
import axios from 'axios'
import AutoHeightImage from 'react-native-auto-height-image';
import DrawerComponent from './../../components/Main/Drawer'
import Drawer from 'react-native-drawer'

let width = Dimensions.get('window').width


export default class TabBarComponentBahraman extends Component {
    constructor(props) {
        super(props);
        this.state = {
          isDrawerOpen: false,
          list:[],
          categoryList:[],
          error: null,
          category:0,
            
        }
    }

    closeControlPanel = () => {
        this._drawer.close()
      };
      onOpenDrawer() {
        this.props.navigation.setParams({isDrawerOpen: true })
      }
      onCloseDrawer() {
        this.props.navigation.setParams({isDrawerOpen: false })
    }
      openControlPanel = () => {
        this._drawer.open()
      };
      componentDidMount() {
        this.props.navigation.setParams({ openDrawer: ()=>this._drawer.open(),closeDrawer: ()=>this._drawer.close(), isDrawerOpen: false })
        this.requestForCategoryList()
        this.requestFromApi();
      }

    requestForCategoryList(){
        axios({
            method: 'get',
            url:`http://5.253.27.167:8000/api/v1/product-cats/`,
            headers:{'content-type': 'application/json',
            'Authorization':'Token 45ad8b095007b39a59542ceac38ef90aace0774f'}   
        })
        .then(res => {
            // console.log(res)
          this.setState({
            categoryList:res.data,
          })
        //   console.log(this.state.categoryList)
        })
        .catch(error => {
          this.setState({ error, loading : false });
          console.log(error);
        })
    }

    requestFromApi(){
        axios({
          method: 'get',
          url:`http://5.253.27.167:8000/api/v1/product/`,
          headers:{'content-type': 'application/json',
          'Authorization':'Token 45ad8b095007b39a59542ceac38ef90aace0774f'}   
        })
        .then(res => {
          this.setState({
          list:res.data,
          })
        })
        .catch(error => {
          this.setState({ error, loading : false });
          console.log(error);
        })
      }
      renderItem=({item,index}) => {
        if(item.category.id==this.state.category || this.state.category==0){
            return(
                // <TouchableOpacity onPress={() => this.props.navigation.navigate('productsDetail')}>
                //     <Product 
                //     pic={item.image.file} 
                //     type={item.name} 
                //     weight={item.weight}
                //     />
                // </TouchableOpacity>
                <View style={styles.productStyle}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('productsDetail' ,
                    {pic:item.image.file ,name:item.name ,weight:item.weight})}>
                        <View style={styles.picStyle}>
                            <AutoHeightImage width={width*0.4} source={{uri:`${item.image.file}`}} style={styles.picStyle}/>
                        </View>
                        <Text style={styles.namestyle}>{item.name}</Text>
                        <Text style={{textAlign:'center',color:'rgb(179, 179, 179)'}}>{item.weight} گرم</Text>
                    </TouchableOpacity>
                </View>
            )}
        }
    //   componentDidMount() {
    //     this.requestForCategoryList()
    //     this.requestFromApi();
        
    //   }
    
  render() {
    return (
        <Drawer
        navigation={this.props.navigation}
        side={"right"}
        ref={(ref) => this._drawer = ref}
        type="overlay"
        content={<DrawerComponent/>}
        tapToClose={true}
        onOpen={()=>this.onOpenDrawer()}
        onClose={()=>this.onCloseDrawer()}
        openDrawerOffset={0.2} // 20% gap on the left side of drawer
        panCloseMask={0.2}
        closedDrawerOffset={-3}
        styles={drawerStyles}
        tweenHandler={(ratio) => ({
            main: { opacity:(2-ratio)/2 }
        })}
        >
        <View style={{flex:1,width:'100%',height:'100%',flexDirection: 'column'}}>
            <View style={[styles.tabBar,styles.FLEX_RR_N_SB_C_S]}>
                <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
                    <View style={{flexDirection:'row',alignItems: 'center',}}>
                        <TouchableOpacity onPress={()=>this.setState({category:0})} activeOpacity={.8} style={styles.tabBarItem}>
                            <Text style={[styles.tabBarIcon,{color:this.props.navigation.state.index == 0 ? '#ad8838' :"rgb(179, 179, 179)'"}]}>همه</Text>        
                        </TouchableOpacity>
                        <View style={styles.spacer}></View>
                    </View>
                    {this.state.categoryList.map((data,index) => {
                        return(
                            <View style={{flexDirection:'row',alignItems: 'center',}}>
                                <TouchableOpacity onPress={()=>this.setState({category:data.id})} activeOpacity={.8} style={styles.tabBarItem}>
                                    <Text style={[styles.tabBarIcon,{color:this.props.navigation.state.index == 0 ? '#ad8838' :"rgb(179, 179, 179)'"}]}>{data.name}</Text>        
                                </TouchableOpacity>
                                <View style={styles.spacer}></View>
                            </View>
                        )
                    })
                    }
                </ScrollView>
                {/* <TouchableOpacity onPress={()=>this.props.navigation.navigate("zaferan")} activeOpacity={.8} style={styles.tabBarItem}>
                    <Text style={[styles.tabBarIcon,{color:this.props.navigation.state.index == 0 ? 'rgb(203,168,95)' :"#959595"}]}>زعفران</Text>        
                </TouchableOpacity>
                <View style={styles.spacer}></View>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("nabat")} activeOpacity={.8} style={styles.tabBarItem}>
                    <Text style={[styles.tabBarIcon,{color:this.props.navigation.state.index == 1 ? 'rgb(203,168,95)' :"#959595"}]}>نبات</Text>        
                </TouchableOpacity>
                <View style={styles.spacer}></View>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("advie")} activeOpacity={.8} style={styles.tabBarItem}>
                    <Text style={[styles.tabBarIcon,{color:this.props.navigation.state.index == 2 ? 'rgb(203,168,95)' :"#959595"}]}>ادویه</Text>        
                </TouchableOpacity>
                <View style={styles.spacer}></View>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("other")} activeOpacity={.8} style={styles.tabBarItem}>
                    <Text style={[styles.tabBarIcon,{color:this.props.navigation.state.index == 3 ? 'rgb(203,168,95)' :"#959595"}]}>دیگرمحصولات</Text>
                </TouchableOpacity> */}
            </View>
            <FlatList
                contentContainerStyle={styles.container}
                data={this.state.list}
                extraData={this.state}
                renderItem={this.renderItem}
            /> 
        </View>
        </Drawer>
    )
  }
}
const drawerStyles = {
    drawer: {},
    main: {paddingLeft: 3},
}
const styles = StyleSheet.create({
    spacer: {
        height:20,
        backgroundColor:'#f3ebd8',
        width:2,
        borderRadius:10
    },
    container: {
        // flex:1,
        // backgroundColor:'red',
        left:(width-(width*0.8+40))/2,
        flexDirection:'row',
        alignItems: 'center',
        flexWrap:'wrap',
        // justifyContent:'center',
        // alignContent:'stretch'
  
      },
    tabBarIcon: {
        fontSize: RF(1.8),
        textAlign:'center'
    },
    tabBarItem: {
        paddingVertical:10,
        width: (WIN.width-20)/4
    },
    tabBar: {
        backgroundColor:'rgb(255, 255, 255)',
        width:'100%',
        shadowRadius:20,
        shadowOpacity:.4,
        shadowColor:'#b7b7b7',
        shadowOffset:{width:0,height:-14},
        paddingBottom:0
    },
    FLEX_RR_N_SB_C_S :{
        display: 'flex',
        flexDirection: 'row-reverse',
        flexWrap: 'nowrap',
        justifyContent: 'space-between',
        alignItems: 'center',
        alignContent: 'stretch',
    },
    productStyle:{
        paddingTop:1,
        backgroundColor:'white',
        width:width*0.4,
        height:width*0.42,
        marginTop:20,
        margin:10,
        borderRadius:7,
        shadowOffset:{  width: 0,  height:0,  },
        shadowColor: '#b7b7b7',
        shadowOpacity: .5,
        // shadowRadius:2,

    },
    picStyle:{
        // marginTop:20,
        // padding:30,
        // borderRadius:20,
        // borderTopRightRadius:7,
        alignItems: 'center',
        justifyContent:'center',
        
        height:width*0.32,
        // borderTopLeftRadius:20
    },
    namestyle:{
        textAlign:'center',
        color:'#ad8838',
        fontWeight: "bold"
    },
})