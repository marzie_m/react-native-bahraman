import React, { Component } from 'react';
import {View ,Text,TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import RF from 'react-native-responsive-fontsize'
import NavigationServices from './../../navigation-service'

class Header extends React.Component {
    render() {
        return (
            <View style={[{position:'relative',backgroundColor:this.props.color},styles.topBar]}>
                <Text style={[{color:this.props.textColor,fontFamily:'Mj_Flow'},styles.topBarText]}>
                {this.props.title}
                </Text>
                {/* <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,right:20}}
                onPress={()=> props.scene.route.params.isDrawerOpen ? props.scene.route.params.closeDrawer() : props.scene.route.params.openDrawer()}>
                    <Icon name={props.scene.route.params.isDrawerOpen ? "times" : "bars"} color={"#aaaaaa"} size={RF(2.5)}/>
                </TouchableOpacity> */}
                <TouchableOpacity activeOpacity={.6} style={{position:'absolute',top:45,left:20}}
                    onPress={() => 
                        // props.navigation.navigation.}
                        NavigationServices.navigate(this.props.BackPage)}
                    >
                    <Icon name='chevron-left' color={"#aaaaaa"} size={RF(2.5)}/>
                </TouchableOpacity>
            </View>
        );
    }
}

export default Header;