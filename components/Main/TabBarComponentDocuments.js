import React, { Component } from 'react'
import {StyleSheet, Text, View,TouchableOpacity,Dimensions,FlatList,ScrollView} from 'react-native'
const WIN = Dimensions.get('window')
import Icon from 'react-native-vector-icons/FontAwesome';
import RF from 'react-native-responsive-fontsize'
import axios from 'axios'
import AutoHeightImage from 'react-native-auto-height-image';
import DrawerComponent from './../../components/Main/Drawer'
import Drawer from 'react-native-drawer'
let width = Dimensions.get('window').width


export default class TabBarComponentDocuments extends Component {
        constructor(props) {
            super(props);
            // this.onBtnPress = this.onBtnPress.bind(this)
            this.state = {
              loading: false,
              list:[],
              categoryList:[],
              error: null,
              refreshing: false,
              isDrawerOpen: false,
              category:0,
              colors:['rgb(84,157,217)','rgb(205,73,165)','rgb(211,141,73)']
                
            }
        }

        requestForCategoryList(){
            axios({
                method: 'get',
                url:`http://5.253.27.167:8000/api/v1/article-cats/`,
                headers:{'content-type': 'application/json',
                'Authorization':'Token 45ad8b095007b39a59542ceac38ef90aace0774f'}   
            })
            .then(res => {
                // console.log(res)
              this.setState({
                categoryList:res.data,
              })
              // console.log(this.state.categoryList)
            })
            .catch(error => {
              this.setState({ error, loading : false });
              console.log(error);
            })
        }


        requestFromApi(){
            axios({
              method: 'get',
              url:`http://5.253.27.167:8000/api/v1/articles/`,
              headers:{'content-type': 'application/json',
              'Authorization':'Token 45ad8b095007b39a59542ceac38ef90aace0774f'}   
          })
          
            .then(res => {
                // console.log(res)
              this.setState({
              list:res.data,
              })
              console.log(this.state.list)
            })
            .catch(error => {
              this.setState({ error, loading : false });
              console.log(error);
            })
          }
          closeControlPanel = () => {
            this._drawer.close()
          };
          onOpenDrawer() {
            this.props.navigation.setParams({isDrawerOpen: true })
          }
          onCloseDrawer() {
            this.props.navigation.setParams({isDrawerOpen: false })
        }
          openControlPanel = () => {
            this._drawer.open()
          };
          componentDidMount() {
            this.requestForCategoryList()
            this.requestFromApi();
            this.props.navigation.setParams({ openDrawer: ()=>this._drawer.open(),closeDrawer: ()=>this._drawer.close(), isDrawerOpen: false })
          }

        renderItem1 = ({item,index}) => {
            if(item.category == this.state.category || this.state.category==0){
            if(index % 2 != 0){
            return(
                <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailOfDOcuments' ,
                {pic:item.image.file ,text:item.title,contents:item.contents})}> 
                    <View style={styles.container}>
                        <AutoHeightImage width={width/2} source={{uri:`${item.image.file}`}} style={styles.pic_style}/>
                         <View style={styles.text_style}>
                            <Text style={{textAlign:'right',fontSize:17}}>{item.title}</Text>
                            <Text style={{textAlign:'right',color:'rgb(136,136,136)',paddingTop:10}} numberOfLines={3}> {item.contents[0].text}</Text>
                        </View>
                    </View>  
                </TouchableOpacity>
            )}
          else{
            return(
                <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailOfDOcuments',
                {pic:item.image.file ,text:item.title,contents:item.contents})}>
                    <View style={styles.container}>
                        <View style={styles.text_style}>
                            <Text style={{textAlign:'right',fontSize:17}}>{item.title}</Text>
                            <Text style={{textAlign:'right',color:'rgb(136,136,136)',paddingTop:10}} numberOfLines={3}> {item.contents[0].text}</Text>
                        </View>
                        <AutoHeightImage width={width/2} source={{uri:`${item.image.file}`}} style={styles.pic_style}/>   
                    </View>
                </TouchableOpacity>
            )}
          }
        //   else if()
        }
    
   
        render() {
            
            return (
              <Drawer
              navigation={this.props.navigation}
              side={"right"}
              ref={(ref) => this._drawer = ref}
              type="overlay"
              content={<DrawerComponent/>}
              tapToClose={true}
              onOpen={()=>this.onOpenDrawer()}
              onClose={()=>this.onCloseDrawer()}
              openDrawerOffset={0.2} // 20% gap on the left side of drawer
              panCloseMask={0.2}
              closedDrawerOffset={-3}
              styles={drawerStyles}
              tweenHandler={(ratio) => ({
                  main: { opacity:(2-ratio)/2 }
              })}
              >
                <View style={{flex:1,width:'100%',height:'100%',flexDirection: 'column'}}>
                    <View style={[styles.tabBar,styles.FLEX_RR_N_SB_C_S]}>
                        <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
                          <TouchableOpacity onPress={()=>this.setState({category:0})} activeOpacity={.8} style={[styles.tabBarItem ,{ backgroundColor:'rgb(84,157,216)'}]}>
                              <Text style={[styles.tabBarIcon,{color:this.props.navigation.state.index == 0 ? 'rgb(203,168,95)' :"white"}]}>همه</Text>        
                          </TouchableOpacity>
                          {this.state.categoryList.map((data,index) => {
                              return(
                                  <TouchableOpacity onPress={()=>this.setState({category:data.id})} activeOpacity={.8} style={{paddingVertical:5,width: (WIN.width)/3,backgroundColor:this.state.colors[(index+1)%3]}}>
                                      <Text style={[styles.tabBarIcon,{color:this.props.navigation.state.index == 0 ? 'rgb(203,168,95)' :"white"}]}>{data.name}</Text>        
                                  </TouchableOpacity>
                              )
                          })
                          }
                        </ScrollView>
                    </View>
                      {/* <View style={{width:'100%',backgroundColor:'green'}}> */}
                        <FlatList
                        data={this.state.list}
                        extraData={this.state}
                        renderItem={this.renderItem1}
                        // onRefresh={this.handleRefresh}
                        // refreshing={this.state.refreshing}
                        />
                      {/* </View> */}
                </View>
              </Drawer>
            
            )
        // })
        }
}
const drawerStyles = {
  drawer: {},
  main: {paddingLeft: 3},
}
const styles = StyleSheet.create({
    spacer: {
        height:'50%',
        // backgroundColor:'#eeeeee',
        width:2,
        borderRadius:10
    },
    tabBarIcon: {
        fontSize: RF(1.8),
        textAlign:'center',
        fontFamily:'Mj_Flow'
        
    },
    tabBarItem: {
        paddingVertical:5,
        width: (WIN.width)/3,
        
        
    },
    tabBar: {
        // backgroundColor:'red',
        // 'rgb(255, 255, 255)',
        width:'100%',
        // height:30
        // shadowRadius:20,
        // shadowOpacity:.4,
        // shadowColor:'#b7b7b7',
        // shadowOffset:{width:0,height:-14},
        // paddingBottom:0
    },
    FLEX_RR_N_SB_C_S :{
        // position:'absolute',
        // height:40,
        // backgroundColor:'green',
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'nowrap',
        justifyContent: 'space-between',
        alignItems: 'center',
        alignContent: 'stretch',
    },
    container: {
        flexDirection:'row',
        alignItems: 'center',
        flexWrap:'wrap',
        justifyContent:'space-between',
        alignContent:'stretch',
        // backgroundColor:'red'
      },
      text_style:{
        justifyContent:'center',
        // alignItems:'center',
        width:width/2,
        height:width/2,
        padding:20,
        // backgroundColor:'green'
      },
      pic_style:{
        height:width/2,
        // backgroundColor:'red'
      }
})