import React, { Component } from 'react';
import {
  ActivityIndicator,
  ImageBackground,
  Text,
  Dimensions,
  View,
  StyleSheet
} from 'react-native';
// import { createIconSetFromFontello } from 'react-native-vector-icons';
// import fontelloConfig from './config.json';
// const Icon = createIconSetFromFontello(fontelloConfig);
let width = Dimensions.get('window').width
let height = Dimensions.get('window').height
import RF from 'react-native-responsive-fontsize'
export default class SplashScreen extends Component{

    componentDidMount() {
        setTimeout(() => {
            this.props.navigation.navigate('page2')
        }, 500)
    }
    render() {
    return (
        <View style={styles.container}>
            <ImageBackground resizeMode={"cover"} source={require('./../../static/images/intro.png')} style={{ width:'100%',height:'100%' }}>
                {/* <Icon color="white" style={{marginLeft:'auto',marginRight:'auto',marginTop:130}} size={RF(13)} name='p'/> */}
                <ActivityIndicator style={{marginTop:40}} color="white"/>
            </ImageBackground>
            {/* <Text>ehc</Text> */}
        </View>
    );
  }
}
const styles = StyleSheet.create({
    container: {
        width:width,
        height:height,
        // flexDirection: 'row',
        // backgroundColor:'red',
        // flexWrap: 'wrap',
        flex: 1,
        // backgroundColor:'white'
    },

})
