import React, { Component } from 'react';
import RF from 'react-native-responsive-fontsize'
import {
  Platform,
  StyleSheet,
  Keyboard,
  Text,
  View,
  Alert,
  TextInput,
  ImageBackground,
  TouchableOpacity,
  AsyncStorage,
  AppRegistry,
  TouchableWithoutFeedback
} from 'react-native';
// import ScannerPage from './ScannerPage';
import axios from 'axios';

class SignUp extends Component {
    constructor(){
        super();
        this.state = {
            phone: '',
            error: '',
            firstName:''
        }
    }
    requestLoginFromApi(){
        if(this.state.phone.length > 0) {
        axios.post(
            // method: 'post',
            `http://5.253.27.167:8000/api/v1/auth/register/`,{
                'phone':this.state.phone,
                'first_name':this.state.firstName
            },
            {
            headers:{'content-type': 'application/json',
            'Authorization':'Token 45ad8b095007b39a59542ceac38ef90aace0774f'},   
            // data:{
                
            // }
        })
        .then(res=>{
            console.log(res.request.response)
        })
        .then(
            ()=>this.props.navigation.navigate('page2')
        )
        .catch(error => {
            // this.setState({ error, loadin});
            console.log(error);
          })
    }
}
    render() {
        return (
            <View style={styles.center}>
                <TextInput
                clearButtonMode={'while-editing'}
                keyboardAppearance={"dark"}
                returnKeyType="next"
                style={styles.inputStyle}
                placeholder="phone"
                autoCapitalize = 'none'
                onChangeText={(text) => this.setState({phone: text})}
                onSubmitEditing={() => { this.secondTextInput.focus(); }}
                blurOnSubmit={false}
                enablesReturnKeyAutomatically
                pointerEvents="auto"
                value={this.state.phone}
                />
                {/* <TextInput
                clearButtonMode={'while-editing'}
                keyboardAppearance={"dark"}
                returnKeyType="next"
                style={styles.inputStyle}
                placeholder="first name"
                autoCapitalize = 'none'
                onChangeText={(text) => this.setState({firstName: text})}
                onSubmitEditing={() => { this.secondTextInput.focus(); }}
                blurOnSubmit={false}
                enablesReturnKeyAutomatically
                pointerEvents="auto"
                value={this.state.firstName}
                /> */}
                <TouchableOpacity
                style={styles.enter} 
                    onPress={()=>this.requestLoginFromApi()} 
                >
                    <Text style={{textAlign:'center' ,color:'#F4F8FB' ,fontSize:RF(2.8),}}>sign up</Text>
                </TouchableOpacity>
            </View>

        );
    }
}
const styles=StyleSheet.create({
    username:{
      // backgroundColor:'#e6e6ff'
    },
     enter:{
          // flex:1,
          width:'90%',
          paddingVertical:16,
          justifyContent:'center' ,
          alignItems:'center' ,
          marginTop:10,
          marginLeft:'10%',
          marginRight:'10%',
          borderRadius:5,
          backgroundColor:'#2E4FAE'
     },
     inputStyle:{
          // backgroundColor:'#3F6BE8',
        //   backgroundColor:'rgba(63,107,232,1)',
          width:'90%',
          textAlign: 'center',
          paddingVertical:15,
          marginTop:10,
          borderRadius:7,
          borderColor:'#2E4FAE',
          borderWidth:1,
          fontSize:RF(3.1),
          color:'white'
     },
     center:{
          flex:1,
          width:'100%',
        //   top:105,
          justifyContent:'center' ,
          alignItems:'center',
          padding: 20,
          backgroundColor:'rgb(238,237,239)',
          // left:'10%',
        //   marginTop:20,
          // position:'absolute',
      }
  })

export default SignUp;