import React, { Component } from 'react';
import {View,Text ,Image,StyleSheet,Dimensions,TouchableOpacity,ScrollView} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image';
let width = Dimensions.get('window').width
let height = Dimensions.get('window').height
import RF from 'react-native-responsive-fontsize'
import DrawerComponent from './../../components/Main/Drawer'
import Drawer from 'react-native-drawer'
const baseUrl='http://5.253.27.167:8000'


class DetailOfDOcuments extends Component {
    constructor(){
        super();
        this.state={
            arrayContent:[],
            isDrawerOpen: false,

        }
    }
    closeControlPanel = () => {
        this._drawer.close()
      };
      onOpenDrawer() {
        this.props.navigation.setParams({isDrawerOpen: true })
      }
      onCloseDrawer() {
        this.props.navigation.setParams({isDrawerOpen: false })
    }
      openControlPanel = () => {
        this._drawer.open()
      };
    componentDidMount(){
        this.setState({arrayContent:this.props.navigation.getParam('contents')})
        this.props.navigation.setParams({ openDrawer: ()=>this._drawer.open(),closeDrawer: ()=>this._drawer.close(), isDrawerOpen: false })

    }
    lapsList() {
        return this.state.arrayContent.map((data,index) => {
            if(data.image == null){
                return(
                    <View style={{paddingBottom:20}}>
                        <Text style={{textAlign:'right',color:'rgb(136,136,136)'}}>{data.text}</Text>
                    </View>
                )
            }
            else if(data.text == null){
                return(
                    <View style={{paddingBottom:20}}>
                        <Image style={{width:100,height:100}} source={{uri:baseUrl+`${data.image.file}`}}/>                    
                    </View>
                )
            }
            else{
                return(
                    <View style={{paddingBottom:20,justifyContent:'space-between'}}>
                        <Text style={{textAlign:'right',color:'rgb(136,136,136)'}}>{data.text}</Text>
                        <View style={{width:width-60,justifyContent:'center',alignItems:'center',marginTop:10}}>
                            <Image style={{width:width-70,height:200}} source={{uri:baseUrl+`${data.image.file}`}}/>                    
                        </View>
                    </View>
                )
            }

            // if(data.image == null){
            //     return (
            //         <View style={{paddingBottom:20}}>
            //             <View style={{paddingHorizontal:10,width:(width/1.2)-15}}>
            //                 <Text style={{textAlign:'right',color:'rgb(136,136,136)'}}>{data.text}</Text>
            //             </View>
            //             {/* <Text style={{textAlign:'right',color:'red',fontSize:23}}>{index+1}</Text> */}
            //         </View>
            //     )
            // }
            // else if(data.text == null){
            //     return (
            //         <View style={{paddingBottom:20}}>
            //             <View style={{paddingHorizontal:10,width:(width/1.2)-15}}>
            //                 <Image style={{width:100,height:100}} source={{uri:baseUrl+`${data.image.file}`}}/>
            //             </View>
            //             {/* <Text style={{textAlign:'right',color:'red',fontSize:23}}>{index+1}</Text> */}
            //         </View>
            //     )

            // }
            // else{
            //     return (
            //         <View style={{paddingBottom:20}}>
            //             {/* <View style={{paddingHorizontal:10,width:(width/1.2)-15}}> */}
            //                 <Text style={{textAlign:'right',color:'rgb(136,136,136)'}}>{data.text}</Text>
            //                 {/* <View style={{width:(width/1.2)-20,height:210,padding:13,marginTop:10,marginLeft:5,backgroundColor:'white',shadowColor: '#b7b7b7',shadowOpacity: .5}}> */}
            //                     <Image style={{width:(width/1.2)-50,height:190}} source={{uri:baseUrl+`${data.image.file}`}}/>
            //                 {/* </View> */}
            //             {/* </View> */}
            //             {/* <Text style={{textAlign:'right',color:'red',fontSize:23}}>{index+1}</Text> */}
            //         </View>
            //     )

            // }




        })
    }

    
    render() {
    const { navigation } = this.props;
    const pic = navigation.getParam('pic')
    const title = navigation.getParam('text')

        return (
            <Drawer
              navigation={this.props.navigation}
              side={"right"}
              ref={(ref) => this._drawer = ref}
              type="overlay"
              content={<DrawerComponent/>}
              tapToClose={true}
              onOpen={()=>this.onOpenDrawer()}
              onClose={()=>this.onCloseDrawer()}
              openDrawerOffset={0.2} // 20% gap on the left side of drawer
              panCloseMask={0.2}
              closedDrawerOffset={-3}
              styles={drawerStyles}
              tweenHandler={(ratio) => ({
                  main: { opacity:(2-ratio)/2 }
              })}
              >
                <ScrollView showsVerticalScrollIndicator={false} style={{height:'100%'}}>
                    <View style={styles.container}>
                        <View style={styles.picStyle}>
                            <Image source={{uri:`${pic}`}} style={{width:width,height:'100%',}}/>
                        </View>
                        <Text style={{textAlign:'right',paddingHorizontal:30,paddingTop:20,fontFamily:'Mj_Flow',fontSize:17}}>{title}</Text>
                        <View style={{paddingHorizontal:30,paddingVertical:25}}>{this.lapsList()}</View>
                    </View>
                </ScrollView>
            </Drawer>
        );
    }
    
}
const drawerStyles = {
    drawer: {},
    main: {paddingLeft: 3},
  }
const styles=StyleSheet.create({
    container:{
        width:'100%',
        height:'100%',
        paddingBottom:500,
        // shadowOffset:{  width: 0,  height:0,  },
        // shadowColor: '#b7b7b7',
        // shadowOpacity: .5,
        // top:-20,
        // justifyContent:'center',
        // alignItems:'center',
        // backgroundColor:'red',


    },
    picStyle:{
        
        alignItems: 'center',
        justifyContent:'center',
        width:width,
        height:250,
    },
    pokht:{
        flexDirection: 'column',
        justifyContent: 'space-between',
        paddingTop:10,
        width:(width/1.2)+20,
      }


})

export default DetailOfDOcuments;