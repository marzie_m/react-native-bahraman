import React, { Component } from 'react'
import {StyleSheet,ScrollView,ImageBackground,Text, View,TouchableOpacity,Dimensions,Image} from 'react-native'
const WIN = Dimensions.get('window')
import Icon from 'react-native-vector-icons/FontAwesome';
import RF from 'react-native-responsive-fontsize'
import AutoHeightImage from 'react-native-auto-height-image';
import DrawerComponent from './../../components/Main/Drawer'
import Drawer from 'react-native-drawer'
// const farangi = require("./../static/images/")
const BahramanBG = require("./../../static/images/bahraman-bg.png");
const url = require('./../../static/images/mahsoolat/bahraman-mahsoolat-detail-withoutHeader.png')
// const ss=require('./../../static/images/saffron copy.png')
const pp=require('./../../static/images/pp.png')
const sargol=require('./../../static/images/second.psd')

export default class Knowledge extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isDrawerOpen: false,
            background_pic:'',
        }
    }
    closeControlPanel = () => {
        this._drawer.close()
      };
      onOpenDrawer() {
        this.props.navigation.setParams({isDrawerOpen: true })
      }
      onCloseDrawer() {
        this.props.navigation.setParams({isDrawerOpen: false })
    }
      openControlPanel = () => {
        this._drawer.open()
      };
      componentDidMount() {
        this.props.navigation.setParams({ openDrawer: ()=>this._drawer.open(),closeDrawer: ()=>this._drawer.close(), isDrawerOpen: false })
      }
    render() {
        return (
        <Drawer
        side={"right"}
        ref={(ref) => this._drawer = ref}
        type="overlay"
        content={<DrawerComponent navigation={this.props.navigation}/>}
        tapToClose={true}
        onOpen={()=>this.onOpenDrawer()}
        onClose={()=>this.onCloseDrawer()}
        openDrawerOffset={0.2} // 20% gap on the left side of drawer
        panCloseMask={0.2}
        closedDrawerOffset={-3}
        styles={drawerStyles}
        tweenHandler={(ratio) => ({
            main: { opacity:(2-ratio)/2 }
        })}
        >
            <View style={styles.container}>
            <ImageBackground source={this.state.background_pic} resizeMode={"cover"} style={styles.BahramanBg}>
                <View style={styles.zaferan_section}>
                    <TouchableOpacity onPress={()=> this.setState({background_pic:url})}>
                    {/* <Image style={{height:10,right:10}} source={pp}/> */}



                    {/* <View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center'}}> */}
                        <Text style={styles.itemStyle}>نگین</Text>
                        {/* <Image source={ss} style={{backgroundColor:'blue' ,width:70 ,height:70}}/> */}
                    {/* </View> */}
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=> this.setState({background_pic:sargol})}>
                        <Text style={styles.itemStyle}>سرگل</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=> this.setState({background_pic:pp})}>
                        <Text style={styles.itemStyle}>پوشال</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=> this.setState({background_pic:url})}>
                        <Text style={styles.itemStyle}>دسته</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=> this.setState({background_pic:url})}>
                        <Text style={styles.itemStyle}>کنج</Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
            </View>
        </Drawer> 
        );
    }
}

const drawerStyles = {
    drawer: {},
    main: {paddingLeft: 3},
}
const styles = StyleSheet.create({
    zaferan_section:{
        // right:10,
        // backgroundColor:'red',
        paddingRight:40
        // width:(WIN.width),
        // height:'70%',
        // paddingTop:200,
        // marginLeft:(WIN.width)/2,
        // justifyContent:'flex-end',
        // alignItems:'center'
    },
    itemStyle:{
        padding:20,
        textAlign:'right'
    },
    BahramanBg:{
        width: '100%',
        height: '100%',
        justifyContent:'center'
    },
    container: {
        // justifyContent:'flex-end',
        flex: 1,
        backgroundColor:'white',
        justifyContent:'center',
        // alignItems:'center'

    },
})
