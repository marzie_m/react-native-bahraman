import React, { Component } from 'react';
import {View,Text ,Image,StyleSheet,Dimensions,TouchableOpacity} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image';
let width = Dimensions.get('window').width
let height = Dimensions.get('window').height
import RF from 'react-native-responsive-fontsize'
import NavigationServices from './../../navigation-service'
const header=require('./../../static/images/mahsoolat/islamic-top-line.png')
const eslimi=require('./../../static/images/mahsoolat/Layer 4.png')
import DrawerComponent from './../../components/Main/Drawer'
import Drawer from 'react-native-drawer'



class ProductDetail extends Component {
    constructor(){
        super();
        this.state={
            isDrawerOpen: false,

        }
    }
    
    closeControlPanel = () => {
        this._drawer.close()
      };
      onOpenDrawer() {
        this.props.navigation.setParams({isDrawerOpen: true })
      }
      onCloseDrawer() {
        this.props.navigation.setParams({isDrawerOpen: false })
    }
      openControlPanel = () => {
        this._drawer.open()
      };
      componentDidMount(){
        this.props.navigation.setParams({ openDrawer: ()=>this._drawer.open(),closeDrawer: ()=>this._drawer.close(), isDrawerOpen: false })
    }
    render() {
    const { navigation } = this.props;
    const pic = navigation.getParam('pic')
    const name=navigation.getParam('name')
    const weight=navigation.getParam('weight')
        return (
            <Drawer
            navigation={this.props.navigation}
            side={"right"}
            ref={(ref) => this._drawer = ref}
            type="overlay"
            content={<DrawerComponent/>}
            tapToClose={true}
            onOpen={()=>this.onOpenDrawer()}
            onClose={()=>this.onCloseDrawer()}
            openDrawerOffset={0.2} // 20% gap on the left side of drawer
            panCloseMask={0.2}
            closedDrawerOffset={-3}
            styles={drawerStyles}
            tweenHandler={(ratio) => ({
                main: { opacity:(2-ratio)/2 }
            })}
            >
            <View style={styles.container}>
            {/* <AutoHeightImage source={header} style={styles.headerStyle}/> */}
                <AutoHeightImage source={{uri:`${pic}`}} style={styles.picStyle}/>
                <View style={styles.textContainer}>
                    <Text style={styles.namestyle}>{name}</Text>
                </View>
                    <Text style={styles.weightstyle}>{weight} گرم</Text>
                 <AutoHeightImage source={eslimi} style={styles.eslimi}/>
            </View>
            </Drawer>
        );
    }
    
}
const drawerStyles = {
    drawer: {},
    main: {paddingLeft: 3},
  }
const styles=StyleSheet.create({
    container:{
        width:'100%',
        height:'100%',
        shadowOffset:{  width: 0,  height:0,  },
        shadowColor: '#b7b7b7',
        shadowOpacity: .5,
        top:-20

    },
    picStyle:{
        
        alignItems: 'center',
        justifyContent:'center',
        width:'100%',
        height:'60%',
    },

    headerStyle:{
        alignItems: 'center',
        justifyContent:'center',
        width:'100%',
        height:'10%',
        marginTop:10
        // position:'absolute'
    },
    namestyle:{
        fontWeight: "bold",
        textAlign:'center',
        color:'#eeeeee',
        fontSize:RF(3.7),
        fontFamily:'Mj_Flow-Bold'
    },
    weightstyle:{
        fontWeight: "bold",
        textAlign:'center',
        color:'rgb(179, 179, 179)',
        fontSize:RF(3.7),
        fontFamily:'Mj_Flow-Bold'

    },
    textContainer:{
        backgroundColor:'rgb(203,168,95)',
        width:'100%',
        height:'10%',
        alignItems: 'center',
        justifyContent:'center',

    },
    eslimi:{
        marginTop:70,
        // backgroundColor:'red',
        // marginBottom:-1,
        // position:abso
        bottom:1,
        width:'100%',
        height:'17%',
    }
})

export default ProductDetail;