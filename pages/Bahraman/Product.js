import React, { Component } from 'react';
import {View,Text ,Image,StyleSheet,Dimensions,TouchableOpacity} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image';
import NavigationServices from './../../navigation-service'
let width = Dimensions.get('window').width

class Product extends Component {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => NavigationServices.navigate('productDetail' ,
                {pic:this.props.pic ,type:this.props.type,weight:this.props.weight})}>
                    <View style={styles.picStyle}>
                        <AutoHeightImage width={width*0.4} source={{uri:`${this.props.pic}`}} style={styles.picStyle}/>
                    </View>
                    <Text style={styles.namestyle}>{this.props.type}</Text>
                    <Text style={{textAlign:'center',color:'rgb(179, 179, 179)'}}>{this.props.weight} گرم</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles=StyleSheet.create({
    container:{
        paddingTop:1,
        backgroundColor:'white',
        width:width*0.4,
        height:width*0.42,
        marginTop:20,
        margin:10,
        borderRadius:7,
        shadowOffset:{  width: 0,  height:0,  },
        shadowColor: '#b7b7b7',
        shadowOpacity: .5,
        // shadowRadius:2,

    },
    picStyle:{
        // marginTop:20,
        // padding:30,
        // borderRadius:20,
        // borderTopRightRadius:7,
        alignItems: 'center',
        justifyContent:'center',
        
        height:width*0.32,
        // borderTopLeftRadius:20
    },
    namestyle:{
        textAlign:'center',
        color:'rgb(203,168,95)',
        fontWeight: "bold"
    },
    // picStyle:{
    //     // marginTop:10,
    //     width:width*0.4,
    //     height:width*0.32,
    //     borderTopLeftRadius:7
    // }

})
export default Product;