import React, { Component } from 'react';
import {View,Text,ScrollView,StyleSheet,Dimensions,Image} from 'react-native'
import AutoHeightImage from 'react-native-auto-height-image';
import DrawerComponent from './../../components/Main/Drawer'
import Drawer from 'react-native-drawer'

let width = Dimensions.get('window').width
let height = Dimensions.get('window').height
const baseUrl='http://5.253.27.167:8000'
// I18nManager.forceRTL(true); 
// {writingDirection: I18nManager.isRTL ? 'rtl' : 'ltr'}
class DetailOfDetailCooking extends Component {
    constructor(){
        super();
        this.state={
            arrayContent:[],
            comments:[],
            ingredientsArray:[],
            isDrawerOpen: false,

        }
    }
    closeControlPanel = () => {
        this._drawer.close()
      };
      onOpenDrawer() {
        this.props.navigation.setParams({isDrawerOpen: true })
      }
      onCloseDrawer() {
        this.props.navigation.setParams({isDrawerOpen: false })
    }
      openControlPanel = () => {
        this._drawer.open()
      };
    componentDidMount(){
        this.setState({arrayContent:this.props.navigation.getParam('contents')})
        this.setState({comments:this.props.navigation.getParam('comments')})
        this.props.navigation.setParams({ openDrawer: ()=>this._drawer.open(),closeDrawer: ()=>this._drawer.close(), isDrawerOpen: false })

        // this.setState({ingredientsArray:this.props.navigation.getParam('ingredients')})
        // console.log(this.state.ingredients)
    }
    ingredients(){
         
            return(
                <View style={styles.pokht}>
                {this.state.ingredientsArray.map((data,index) => (
                    <Text style={{textAlign:'right',color:'rgb(136,136,136)'}}>{data}</Text>
                ))}
                </View>
                
            )}
    sendMessage(){
        
    }
    difficulty(){
        if(`${this.props.navigation.getParam('difficulty')}` == 'normal'){
            <Text style={{textAlign:'right',color:'rgb(136,136,136)',fontFamily:'Mj_Flow'}}>نرمال</Text>
        }
        else if(this.props.navigation.getParam('difficulty') == 'easy'){
            <Text style={{textAlign:'right',color:'rgb(136,136,136)',fontFamily:'Mj_Flow'}}>اسان</Text>
        }
        else if(this.props.navigation.getParam('difficulty') == 'hard'){
            <Text style={{textAlign:'right',color:'rgb(136,136,136)',fontFamily:'Mj_Flow'}}>سخت</Text>
        }
        
    }
    lapsList() {
        return this.state.arrayContent.map((data,index) => {
            if(data.image == null){
                return (
                    <View style={styles.pokht}>
                        <View style={{paddingHorizontal:10,width:(width/1.2)-15}}>
                            <Text style={{textAlign:'right',color:'rgb(136,136,136)'}}>{data.text}</Text>
                        </View>
                        <Text style={{textAlign:'right',color:'red',fontSize:23}}>{index+1}</Text>
                    </View>
                )
            }
            else if(data.text == null){
                return (
                    <View style={[styles.pokht ,{backgroundColor:'red'}]}>
                        <View style={{paddingHorizontal:10,width:(width/1.2)-15}}>
                            <Image style={{height:190}} source={{uri:baseUrl+`${data.image.file}`}}/>
                        </View>
                        <Text style={{textAlign:'right',color:'red',fontSize:23}}>{index+1}</Text>
                    </View>
                )

            }
            else{
                // console.log(data.image.file)
                return (
                    <View style={styles.pokht}>
                        <View style={{paddingHorizontal:10,width:(width/1.2)-15}}>
                            <Text style={{textAlign:'right',color:'rgb(136,136,136)'}}>{data.text}</Text>
                            <View style={styles.imageStyle}>
                                <Image style={{height:190,width:(width/1.2)-60}} source={{uri:baseUrl+`${data.image.file}`}}/>
                            </View>
                        </View>
                        <Text style={{textAlign:'right',color:'red',fontSize:23}}>{index+1}</Text>
                    </View>
                )

            }
        })
    
    }
    render() {
        // console.log(this.state.ingredients)
        return (
            <Drawer
            side={"right"}
            ref={(ref) => this._drawer = ref}
            type="overlay"
            content={<DrawerComponent navigation={this.props.navigation}/>}
            tapToClose={true}
            onOpen={()=>this.onOpenDrawer()}
            onClose={()=>this.onCloseDrawer()}
            openDrawerOffset={0.2} // 20% gap on the left side of drawer
            panCloseMask={0.2}
            closedDrawerOffset={-3}
            styles={drawerStyles}
            tweenHandler={(ratio) => ({
                main: { opacity:(2-ratio)/2 }
            })}
            >
                <ScrollView style={{height:'100%'}}>
                    <View style={styles.container}>
                        <View style={styles.allOfInfo}>
                            <Text style={{textAlign:'right',fontFamily:'Mj_Flow',fontSize:20}}>{this.props.navigation.getParam('title')}</Text>
                            <View style={styles.info}>
                                <View style={{paddingHorizontal:2.5,width:(width/6)}}>{this.difficulty()}</View>
                                <View style={{paddingHorizontal:2.5,width:(width/6)}}>
                                    <Text style={{textAlign:'right',color:'rgb(136,136,136)',fontFamily:'Mj_Flow'}}>{this.props.navigation.getParam('covering')} نفر</Text>
                                </View>
                                <View style={{paddingHorizontal:2.5,width:(width/6)}}>
                                    <Text style={{textAlign:'right',color:'rgb(136,136,136)',fontFamily:'Mj_Flow'}}>{this.props.navigation.getParam('time')} دقیقه</Text>
                                </View>  
                            </View>
                        </View>
                        <Image style={styles.photo} source={{uri:`${this.props.navigation.getParam('pic')}`}}/>
                        <View style={styles.contentStyle}>
                            <View style={{paddingTop:10}}>
                                <Text style={{textAlign:'right',fontFamily:'Mj_Flow',fontSize:18}}>مواد لازم</Text>
                                {/* <View >
                                    {this.ingredients()}
                                    <Text style={{textAlign:'left',color:'rgb(136,136,136)'}}>{this.props.navigation.getParam('ingredients')}</Text>
                                </View> */}
                                <View style={styles.pokht}>
                                    {this.state.ingredientsArray.map((data,index) => {
                                    <Text style={{textAlign:'right',color:'rgb(136,136,136)'}}>{data}</Text>
                                    })}
                                </View>
                            </View>
                            <View style={{paddingTop:10}}>
                                <Text style={{textAlign:'right',fontFamily:'Mj_Flow',fontSize:18}}>دستور پخت</Text>
                                <View >{this.lapsList()}</View>
                            </View>
                            <View style={{paddingTop:10}}>
                                <Text style={{textAlign:'right',fontFamily:'Mj_Flow',fontSize:18}}>نظرات شما</Text>
                                <View style={styles.pokht}></View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </Drawer>
        );
    }
}
const drawerStyles = {
    drawer: {},
    main: {paddingLeft: 3},
  }
const styles = StyleSheet.create({
    container:{
        width:'100%',
        height:'100%',
        paddingBottom:100,
        // backgroundColor:'green',
        // justifyContent: 'center',
        // alignItems: 'center'
    
      },
    allOfInfo:{
        // right:10,
        paddingTop:5,
        paddingRight:20,
        flexDirection: 'column',
        // justifyContent: 'center',
        // alignItems: 'center',
        // backgroundColor:'red'
    },
    info:{
        // marginRight:30,
        paddingVertical:5,
        paddingRight:20,
        // right:10,
        left:(width/2)-20,
        width:width,
        height:40,
        // backgroundColor:'yellow',
        flexDirection: 'row',
        // justifyContent: 'center',
        // alignItems: 'center'
    
      },
      contentStyle:{
        paddingRight:15,
        marginTop:30,
        marginRight:30,
        marginLeft:15,
        // backgroundColor:'green',
        // width:width,
        borderRightColor:'rgb(230,230,230)',
        borderRightWidth:.5,
      },
      photo:{
        // backgroundColor:'green',
        width: width,
        minHeight: height/3.5,
        maxHeight: height/3.5,
      },
      pokht:{
        // backgroundColor:'blue',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop:10,
        width:(width/1.2)-10,
        marginRight:20
        // maxHeight:width/1.2
      },
      imageStyle:{
        height:210,
        padding:13,
        marginTop:10,
        marginLeft:5,
        backgroundColor:'white',
        shadowColor: '#b7b7b7',
        shadowOpacity: .5,
        shadowRadius:10,
        justifyContent:'center',
        alignItems:'center'
      }

})

export default DetailOfDetailCooking;