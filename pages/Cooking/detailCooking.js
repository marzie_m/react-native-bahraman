import React, { Component } from 'react';
import {View,Text,TouchableOpacity,FlatList,Dimensions,StyleSheet,Image} from 'react-native'
import axios from 'axios'
import AutoHeightImage from 'react-native-auto-height-image';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import DrawerComponent from './../../components/Main/Drawer'
import Drawer from 'react-native-drawer'


let width = Dimensions.get('window').width
let height = Dimensions.get('window').height

// import { TouchableOpacity } from 'react-native-gesture-handler';
class detailCooking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            list:[],
            error: null,
            refreshing: false,
            isDrawerOpen: false,
            
        }
    }
    requestFromApi(){
        axios({
          method: 'get',
          url:`http://5.253.27.167:8000/api/v1/recipes/`,
          headers:{'content-type': 'application/json',
          'Authorization':'Token 45ad8b095007b39a59542ceac38ef90aace0774f'}   
      })
        .then(res => {
            // console.log(res)
          this.setState({
          list:res.data,
          // error: null,
          // loading: false,
          // refreshing: false
          })
        })
        .catch(error => {
          this.setState({ error, loading : false });
          // console.log(error);
        })
      }
      closeControlPanel = () => {
        this._drawer.close()
      };
      onOpenDrawer() {
        this.props.navigation.setParams({isDrawerOpen: true })
      }
      onCloseDrawer() {
        this.props.navigation.setParams({isDrawerOpen: false })
    }
      openControlPanel = () => {
        this._drawer.open()
      };
      componentDidMount() {
        this.requestFromApi();
        this.props.navigation.setParams({ openDrawer: ()=>this._drawer.open(),closeDrawer: ()=>this._drawer.close(), isDrawerOpen: false })

      }
      renderItem = ({item}) => {
        const { navigation } = this.props;
        const category = navigation.getParam('category')
        // console.log(category)
        if(item.category.id == category){
          // console.log('yes')
        
        return(
          <TouchableOpacity onPress={()=>this.props.navigation.navigate("detailOfDetailCooking",
            {pic:item.image.file,title:item.title,difficulty:item.difficulty,
            covering:item.covering,time:item.time ,contents:item.contents,
            ingredients:item.ingredients,comments:item.comments})}>
              <View style={styles.container}>
                <View style={styles.photo}>
                  
                    <Image style={styles.photo} source={{uri:`${item.image.file}`}}/>
                </View>
                <View style={styles.title}>
                  {/* <LinearGradient colors={['#4d4d4d', '#404040', '#333333', '#262626']} style={styles.title}> */}
                    <Text style={{textAlign:'center',fontSize:20,color:'rgb(252,252,252)',fontFamily:'Mj_Flow'}}>{item.title}</Text>
                  {/* </LinearGradient> */}
                </View>
                <View style={styles.info}>
                  <View style={{paddingHorizontal:10,color:'rgb(136,136,136)'}}>
                    <Text style={{fontFamily:'Mj_Flow'}}>{item.difficulty}</Text>
                  </View>
                  <View style={{paddingHorizontal:10,color:'rgb(136,136,136)'}}>
                    {/* <Icon name="cheese" style={{fontSize:10}}/> */}
                    <Text style={{fontFamily:'Mj_Flow'}}>{item.covering}نفر</Text>
                  </View>
                  <View style={{paddingHorizontal:10,color:'rgb(136,136,136)'}}><Text style={{fontFamily:'Mj_Flow'}}>{item.time}دقیقه</Text></View>  
                </View>
            </View>
         </TouchableOpacity>
        )}
      }
    render() {

        return (
          <Drawer
            side={"right"}
            ref={(ref) => this._drawer = ref}
            type="overlay"
            content={<DrawerComponent navigation={this.props.navigation}/>}
            tapToClose={true}
            onOpen={()=>this.onOpenDrawer()}
            onClose={()=>this.onCloseDrawer()}
            openDrawerOffset={0.2} // 20% gap on the left side of drawer
            panCloseMask={0.2}
            closedDrawerOffset={-3}
            styles={drawerStyles}
            tweenHandler={(ratio) => ({
                main: { opacity:(2-ratio)/2 }
            })}
        >
          <FlatList
            data={this.state.list}
            extraData={this.state}
            renderItem={this.renderItem}
            onRefresh={this.handleRefresh}
            refreshing={this.state.refreshing}
          /> 
        </Drawer>
        );
    }
}
const drawerStyles = {
  drawer: {},
  main: {paddingLeft: 3},
}
const styles = StyleSheet.create({
  container:{
    width:width,
    // shadowRadius:width,
    // shadowOpacity:.5,
    // shadowColor:'black',
    // shadowOffset:{width:width,height:-50},
    // backgroundColor:'red',
    justifyContent: 'center',
    alignItems: 'center'

  },
  photo:{
    // backgroundColor:'green',
    width: width,
    minHeight: height/3.5,
    maxHeight: height/3.5,
    // shadowRadius:30,
    // shadowOpacity:1,
    // shadowColor:'black',
    // shadowOffset:{width:width,height:50},


  },
  title:{
    position:'absolute',
    
    // backgroundColor:'blue',
    top:height/4.2,
    width:width,
    height:40,
    // marginBottom:10,
    paddingVertical:5,
    justifyContent: 'center',
    
    
  },
  info:{
    width:width,
    height:50,
    // backgroundColor:'yellow',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'

  }
})

export default detailCooking;