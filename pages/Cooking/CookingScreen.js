import React, { Component } from 'react'
import {StyleSheet,ScrollView,ImageBackground,Text, View,TouchableOpacity,Dimensions,FlatList,Image} from 'react-native'
const WIN = Dimensions.get('window')
import Icon from 'react-native-vector-icons/FontAwesome';
import RF from 'react-native-responsive-fontsize'
import AutoHeightImage from 'react-native-auto-height-image';
import DrawerComponent from './../../components/Main/Drawer'
import Drawer from 'react-native-drawer'
import axios from 'axios'

// const farangi = require("./../static/images/cooking/ashpazi1.png")

export default class CookingScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            list:[],
            isDrawerOpen: false
        }
    }
    requestFromApi(){
        axios({
          method: 'get',
          url:`http://5.253.27.167:8000/api/v1/recipe-cats/`,
          headers:{'content-type': 'application/json',
          'Authorization':'Token 45ad8b095007b39a59542ceac38ef90aace0774f'}   
      })
        .then(res => {
          this.setState({
          list:res.data,
          })
        })
        .catch(error => {
          this.setState({ error, loading : false });
          console.log(error);
        })
      }


    renderItem = ({item,index}) => {
        if((index+1)% 3 == 0){
            return(
                <View style={{width:(WIN.width)-10 ,margin:5,justifyContent:'center',alignItems: 'center'}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailCooking',{category:item.id,name:item.name})}>
                      <Image style={styles.photo2} source={{uri:`${item.image.file}`}}/>
                    </TouchableOpacity>
                </View>
            )}
          else{
            return(
                <View style={{width:((WIN.width)/2)-10 ,margin:5,justifyContent:'center',alignItems: 'center'}}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DetailCooking',{category:item.id,name:item.name})}>
                      <Image style={styles.photo1}  source={{uri:`${item.image.file}`}}/>
                    </TouchableOpacity>
                </View>
            )}
        
      }
    // }
    closeControlPanel = () => {
        this._drawer.close()
      };
      onOpenDrawer() {
        this.props.navigation.setParams({isDrawerOpen: true })
      }
      onCloseDrawer() {
        this.props.navigation.setParams({isDrawerOpen: false })
    }
      openControlPanel = () => {
        this._drawer.open()
      };
      componentDidMount() {
        this.requestFromApi();
        this.props.navigation.setParams({ openDrawer: ()=>this._drawer.open(),closeDrawer: ()=>this._drawer.close(), isDrawerOpen: false })
      }
    render() {
        return (
        <Drawer
            side={"right"}
            ref={(ref) => this._drawer = ref}
            type="overlay"
            content={<DrawerComponent navigation={this.props.navigation}/>}
            tapToClose={true}
            onOpen={()=>this.onOpenDrawer()}
            onClose={()=>this.onCloseDrawer()}
            openDrawerOffset={0.2} // 20% gap on the left side of drawer
            panCloseMask={0.2}
            closedDrawerOffset={-3}
            styles={drawerStyles}
            tweenHandler={(ratio) => ({
                main: { opacity:(2-ratio)/2 }
            })}
        >
        {/* // <ScrollView style={styles.container} bounces={false} stickyHeaderIndices={[0]}>
        //     <View style={[styles.Flex_R_N_SE_B_S,styles.MenuRow]}>
        //         <TouchableOpacity><AutoHeightImage width={(WIN.width)/2} source={farangi}/></TouchableOpacity>
        //         <TouchableOpacity><AutoHeightImage width={(WIN.width/100*70)/2} source={Documents}/></TouchableOpacity>
        //     </View>
        //     </ScrollView> */}

            <FlatList
            contentContainerStyle={styles.container}
            data={this.state.list}
            extraData={this.state}
            renderItem={this.renderItem}
            // onRefresh={this.handleRefresh}
            // refreshing={this.state.refreshing}
            /> 
         </Drawer> 
        );
    }
}

const drawerStyles = {
    drawer: {},
    main: {paddingLeft: 3},
}
const styles = StyleSheet.create({
    MenuImage :{
        height: 35
    },
    container: {
        // flex:1,
        // padding:5,
        // backgroundColor:'red',
        flexDirection:'row',
        // alignItems: 'center',
        flexWrap:'wrap',
        // justifyContent:'center',
        // alignContent:'stretch'
  
      },
      photo1:{
        //   paddingHorizontal:10,
        width:((WIN.width)/2)-10 ,
        height:((WIN.width)-10)/2
         
      },
      photo2:{
        width:(WIN.width)-10 ,
        height:((WIN.width)-10)/2
      }
})
