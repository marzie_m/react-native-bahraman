import React, { Component } from 'react';
import { Text, View, Platform, StyleSheet, StatusBar, Button, TouchableOpacity } from 'react-native';
import Drawer from 'react-native-drawer'
import DrawerComponent from './../components/Main/Drawer'
class Galary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDrawerOpen: false,
            
        }
    }

    requestFromApi(){
      axios({
        method: 'post',
        url:`http://5.253.27.167:8000/api/user/`,
        headers:{'content-type': 'application/json',
        'Authorization':'Token 45ad8b095007b39a59542ceac38ef90aace0774f'},   
        data:{
          
        }
    })
      .then(res => {
        this.setState({
        list:res.data,
        })
      })
      .catch(error => {
        this.setState({ error, loading : false });
        console.log(error);
      })
    }


      setModalVisible(visible) {
      this.setState({modalVisible: visible});
      }
    
      closeControlPanel = () => {
        this._drawer.close()
      };
      onOpenDrawer() {
        this.props.navigation.setParams({isDrawerOpen: true })
      }
      onCloseDrawer() {
        this.props.navigation.setParams({isDrawerOpen: false })
    }
      openControlPanel = () => {
        this._drawer.open()
      };
      componentDidMount() {
        this.props.navigation.setParams({ openDrawer: ()=>this._drawer.open(),closeDrawer: ()=>this._drawer.close(), isDrawerOpen: false })
      }
    render() {
        return (
        <Drawer
            side={"right"}
            ref={(ref) => this._drawer = ref}
            type="overlay"
            content={<DrawerComponent/>}
            tapToClose={true}
            onOpen={()=>this.onOpenDrawer()}
            onClose={()=>this.onCloseDrawer()}
            openDrawerOffset={0.2} // 20% gap on the left side of drawer
            panCloseMask={0.2}
            closedDrawerOffset={-3}
            styles={drawerStyles}
            tweenHandler={(ratio) => ({
                main: { opacity:(2-ratio)/2 }
            })}
        >
   
            <View>
                <Text>Galary</Text>
            </View>
            </Drawer>
        );
    }
}
const drawerStyles = {
    drawer: {},
    main: {paddingLeft: 3},
}

export default Galary;