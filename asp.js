
import React, {Component} from 'react';
import {YellowBox} from 'react-native';
import { createBottomTabNavigator, createAppContainer,createStackNavigator,createSwitchNavigator} from 'react-navigation';

import {logRequests} from 'react-native-requests-logger';

import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from './config.json';
const SJIcon = createIconSetFromFontello(fontelloConfig);


import RF from 'react-native-responsive-fontsize'

import Login from './Pages/Authen/Login'
import AuthCheckScreen from './Pages/Authen/Splash'

// import HomeScreen from './Pages/Home/Home'
import CheckListScreen from './Pages/Home/HomeChecklist'
import UserProfileScreen from './Pages/Profiles/UserProfile';
import AppInfoScreen from './Pages/Profiles/AppInfo'

import SellerProfileScreen from './Pages/Profiles/Shop/SellerProfile';
import SellerNewProductScreen from './Pages/Profiles/Shop/SellerNewProduct';
import SellerNewShopScreen from './Pages/Profiles/Shop/NewShop';
import SellerEditNewShopInfoScreen from './Pages/Profiles/Shop/NewShopEditInfo';
import ShopSuccessCreationScreen from './Pages/Profiles/Shop/ShopSuccessCreation'
import SellerMapScreen from './Pages/map'

import CategoriesScreen from './Pages/Categories/Categories'
import CategoriesSubScreen from './Pages/Categories/CategoriesSub'
import CategoryShopsScreen from './Pages/Categories/CategoryShops'
import ShopScreen from './Pages/Categories/Shop'
import ProductScreen from './Pages/Categories/Product'

import MarketScreen from './Pages/Market/Market'
import MarketProductScreen from './Pages/Market/MarketProduct'
import MarketSearchScreen from './Pages/Market/Search'

import IdeasScreen from './Pages/Ideas'
import IdeasPhotoScreen from './Pages/IdeasPhoto'

import PostsScreen from './Pages/Posts'
import PostsOfCatsScreen from './Pages/PostsOfCat'
import PostScreen from './Pages/Post'

import NavigationService from './navigation-services';

YellowBox.ignoreWarnings([' ']);

const CategoryNavigator = createStackNavigator(
  {
    CategoriesPage: {screen: CategoriesScreen},
    CategoriesSubPage: {screen: CategoriesSubScreen},
    CategoryShopsPage:{screen: CategoryShopsScreen},
    ShopPage: {screen: ShopScreen},
    ProductPage: {screen: ProductScreen},
  }
);
const MarketNavigator = createStackNavigator(
  {
    MarketPage: {screen: MarketScreen},
    MarketSearchPage: {screen: MarketSearchScreen},
    MarketShopPage: {screen: ShopScreen},

    MarketProductPage: {screen: ProductScreen},
    CheckListPage: {screen: CheckListScreen},
    SellerProfilePage: {screen: SellerProfileScreen},
    SellerNewProductPage:{screen: SellerNewProductScreen},

    SellerNewShopPage: {screen: SellerNewShopScreen},
    ShopSuccessCreationPage: {screen: ShopSuccessCreationScreen},
    SellerEditNewShopInfoPage: {screen: SellerEditNewShopInfoScreen},
    SellerMapPage:{screen: SellerMapScreen}
  },
  {navigationOptions: () => ({header: null}),}
);
const ProfileNavigator = createStackNavigator(
  {
    UserProfilePage : {screen: UserProfileScreen},
    AppInfoPage: {screen: AppInfoScreen},

    ProfileShopPage: {screen: ShopScreen},
    ProfileProductPage: {screen: ProductScreen}
  },
  {navigationOptions: () => ({header: null}),}
);
const IdeastNavigator = createStackNavigator(
  {
    IdeasPage: {screen: IdeasScreen},
    IdeasPhotoPage: {screen: IdeasPhotoScreen}
  },
  {navigationOptions: () => ({header: null}),}
);
const PostsNavigator = createStackNavigator(
  {
    PostsPage: {screen: PostsScreen},
    PostsOfCatsPage: {screen: PostsOfCatsScreen},
    PostPage: {screen: PostScreen}
  },
  {navigationOptions: () => ({header: null}),}
);
const TabNavigator = createBottomTabNavigator({ 
  'مطالب': PostsNavigator,
  'ایده ها': IdeastNavigator,
  'بازارچه': MarketNavigator,
  'فروشگاه': CategoryNavigator,
  'علاقه مندی': ProfileNavigator
},{
  initialRouteName: "بازارچه",
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      const { routeName } = navigation.state;
      let iconName;
      if (routeName === 'علاقه مندی') {
        iconName = `star`;
      } else if (routeName === 'فروشگاه') {
        iconName = `shop`;
      } else if (routeName == 'بازارچه') {
        iconName = `market`;
      } else if (routeName == 'ایده ها') {
        iconName = `idea`;
      } else if (routeName == 'مطالب') {
        iconName = `blog`;
      }
      return <SJIcon name={iconName} solid={true} size={horizontal ? 23 : 25} color={tintColor} />;
    },
  }),
  tabBarOptions: {
    activeTintColor: '#D9B42F',
    inactiveTintColor: 'gray',
    tabStyle: {
      borderTopColor: '#D9B42F'
    },
    labelStyle: {
      fontFamily: 'IRANYekanMobile',
      fontSize: RF(1.8),
      paddingBottom: 2
    },
  },
});
const AppNavigator = createSwitchNavigator(
  {
    AuthLoading: AuthCheckScreen,
    Auth: Login,
    App: TabNavigator
  },
  {
    initialRouteName: 'AuthLoading',
  }
);

const AppContainer = createAppContainer(AppNavigator);
export default class All extends Component {
  constructor(props) {
    super(props);
    logRequests();
  }
  componentDidMount() {
  }
  render() {
    return (
      <AppContainer
      ref={navigatorRef => {NavigationService.setTopLevelNavigator(navigatorRef);}}/>
    )
  }
}
IdeastNavigator.navigationOptions  = ({ navigation }) => {
  let tabBarVisible = true;
  let routeName = navigation.state.routes[navigation.state.index].routeName
  if ( routeName == 'IdeasPhotoPage' ) {
      tabBarVisible = false
  }
  return {
      tabBarVisible,
  }
}
MarketNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  let routeName = navigation.state.routes[navigation.state.index].routeName
  if ( routeName == 'SellerMapPage' ) {
      tabBarVisible = false
  }
  return {
      tabBarVisible,
  }
}
